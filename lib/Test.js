const { exec } = require('child_process');

module.exports = exercise => {
    exec(`npm test ${exercise}`, (err, stdout, stderr) => {
    
        if (err) console.error(err)
   
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
    });
};

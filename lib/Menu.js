const inquirer = require('inquirer');
const fs = require('fs');
const marked = require('marked');
const TerminalRenderer = require('marked-terminal');

module.exports = (path, menu) => {
    
    const titles = [];

    marked.setOptions({
        renderer: new TerminalRenderer()
    });
    
    for(let item in menu) {
        titles.push(item);
    }

    const options = [{
        type: 'rawlist',
        name: 'exercise',
        message: '¿Cuál vas a Practicar?',
        choices: titles,
    }];

    inquirer.prompt(options)
        .then(answers => {
            fs.readFile(`${path}/${menu[answers.exercise]}/enunciado.md`, (err, data) => {
                if (err) { throw err; }

                const md = marked(data.toString());

                console.clear();
                console.log(md);

            });
        })
        .catch(err => console.log(err));

}
    

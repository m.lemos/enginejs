// 'use strict';

/**
 * This is the main class, the entry point to sequelize.
*/

class EngineJS {

    constructor(options){
        this.menu = options.menu;
        this.path = options.path;
    }

    start = () => require('./Menu')(this.path, this.menu);
    test = () => require('./Test')(this.path);

}

module.exports = EngineJS;
module.exports.EngineJS = EngineJS;
module.exports.default = EngineJS;

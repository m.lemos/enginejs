'use strict';

/**
  * The entry point.
  *
  * @module EngineJS
*/

module.exports = require('./lib/EngineJS');